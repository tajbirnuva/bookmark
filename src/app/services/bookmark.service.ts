import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class BookmarkService {
  constructor() {}

  public getBookmarkList() {
    return JSON.parse(localStorage.getItem('bookmarkList') || '[]');
  }

  public addBookmark(bookmark: any) {
    let bookmarks = [];
    if (localStorage.getItem('bookmarkList')) {
      bookmarks = JSON.parse(localStorage.getItem('bookmarkList') || '[]');
      bookmarks = [bookmark, ...bookmarks];
    } else {
      bookmarks = [bookmark];
    }
    localStorage.setItem('bookmarkList', JSON.stringify(bookmarks));
  }

  public saveBookForShow(bookmark: any) {
    localStorage.setItem('bookmark', JSON.stringify(bookmark));
  }

  public getBookmark() {
    return JSON.parse(localStorage.getItem('bookmark') || '[]');
  }


}
