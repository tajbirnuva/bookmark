import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  constructor() {}

  public getCategoryList() {
    return JSON.parse(localStorage.getItem('categoryList') || '[]');
  }

  public addCategory(category: any) {
    let categories = [];
    if (localStorage.getItem('categoryList')) {
      categories = JSON.parse(localStorage.getItem('categoryList') || '[]');
      categories = [category, ...categories];
    } else {
      categories = [category];
    }
    localStorage.setItem('categoryList', JSON.stringify(categories));
  }
}
