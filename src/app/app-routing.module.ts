import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddBookmarkComponent } from './components/add-bookmark/add-bookmark.component';
import { AddCategoryComponent } from './components/add-category/add-category.component';
import { BookmarkListComponent } from './components/bookmark-list/bookmark-list.component';
import { ShowBookmarkComponent } from './components/show-bookmark/show-bookmark.component';

const routes: Routes = [
  { path: 'add-category', component: AddCategoryComponent, pathMatch: 'full' },
  { path: 'add-bookmark', component: AddBookmarkComponent, pathMatch: 'full' },
  { path: 'show-bookmark/:title', component: ShowBookmarkComponent, pathMatch: 'full' },
  { path: '', component: BookmarkListComponent, pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
