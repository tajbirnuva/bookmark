import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '../../services/category.service';
import { BookmarkService } from '../../services/bookmark.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-bookmark',
  templateUrl: './add-bookmark.component.html',
  styleUrls: ['./add-bookmark.component.css'],
})
export class AddBookmarkComponent implements OnInit {
  bookmark = {
    title: '',
    url: '',
    category: '',
  };
  categories = [];
  category = '';
  newCategory = false;
  bookMarkForm = new FormGroup({
    title: new FormControl('', Validators.required),
    url: new FormControl('', [
      Validators.required,
      Validators.pattern(
        '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?'
      ),
    ]),
    category: new FormControl('', Validators.required),
  });
  constructor(
    private categoryService: CategoryService,
    private bookmarkService: BookmarkService
  ) {}

  ngOnInit(): void {
    this.categories = this.categoryService.getCategoryList();
  }

  onSubmit() {
    this.bookmark = Object.assign(this.bookmark, this.bookMarkForm.value);
    if (this.newCategory) {
      this.categoryService.addCategory(this.bookMarkForm.value.category);
    }
    this.bookmarkService.addBookmark(this.bookmark);
   
    this.bookMarkForm.reset();
    Swal.fire('Success', 'Bookmark Save successfully', 'success');
   
  }

  newCategoryAdd() {
    this.newCategory = true;
  }
}
