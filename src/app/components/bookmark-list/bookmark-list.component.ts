import { Component, OnInit } from '@angular/core';
import { BookmarkService } from '../../services/bookmark.service';
import { CategoryService } from '../../services/category.service';
interface bookmark {
  title: '';
  url: '';
  category: '';
}
@Component({
  selector: 'app-bookmark-list',
  templateUrl: './bookmark-list.component.html',
  styleUrls: ['./bookmark-list.component.css'],
})
export class BookmarkListComponent implements OnInit {
  categories = [];
  bookmarks: bookmark[] = [];
  bookMark: any;
  constructor(
    private categoryService: CategoryService,
    private bookmarkService: BookmarkService
  ) {}

  ngOnInit(): void {
    this.categories = this.categoryService.getCategoryList();
    this.bookmarks = this.bookmarkService.getBookmarkList();
  }

  showBookmarkDetaila(bookmark: any) {
    this.bookMark = bookmark;
  }

  goToLink(bookmark: any) {
    let url = 'http://localhost:4200/show-bookmark/'+bookmark.title;
    this.bookmarkService.saveBookForShow(bookmark);
    window.open(url, '_blank');
  }
}
