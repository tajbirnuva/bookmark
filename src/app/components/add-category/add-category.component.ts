import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from '../../services/category.service';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {
 
  categoryForm = new FormGroup({
    name: new FormControl('',Validators.required),
  });
  constructor(private categoryService:CategoryService) { }

  ngOnInit(): void {
    let list=[];
   list= this.categoryService.getCategoryList();
   console.log(list)
  }

  onSubmit(){  
    this.categoryService.addCategory(this.categoryForm.value.name)
  }

}
