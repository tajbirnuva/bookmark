import { Component, Input, OnInit } from '@angular/core';
import { BookmarkService } from '../../services/bookmark.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-show-bookmark',
  templateUrl: './show-bookmark.component.html',
  styleUrls: ['./show-bookmark.component.css'],
})
export class ShowBookmarkComponent implements OnInit {
  @Input() bookmark: any;
  
  constructor(
    private bookmarkService: BookmarkService,
    private activeRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
   console.log(this.bookmark);
  
    if(this.bookmark==undefined){
      this.bookmark = this.bookmarkService.getBookmark();
    }  
  }
}
